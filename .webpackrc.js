const fs = require('fs');
const path = require('path');
const lodash = require('lodash');
require('dotenv').config();

const ENV = (process.env.ENV);
const API_URL = (process.env.API_URL);
const DATA_API_URL = (process.env.DATA_API_URL);

const PROXY_TARGET = (process.env.PROXY_TARGET);
const PROXY_PATH = (process.env.PROXY_PATH);

const proxyObj = {};
proxyObj[PROXY_PATH] = {
    "target": PROXY_TARGET,
    "changeOrigin": true,
};

export default {
    "html": {
        "template": "public/index.ejs",
    },
    "define": {
        "ENV": ENV,
        "API_URL": API_URL,
        "DATA_API_URL": DATA_API_URL,
    },
    "alias": lodash.reduce(fs.readdirSync(path.resolve(__dirname, 'src/')), (map, name) => {
        const entry = path.resolve(__dirname, `src/${name}`);
        const stat = fs.statSync(entry);
        if (stat.isDirectory()) {
            map[name] = entry;
        }
        return map;
    }, {}),
    "cssModulesExcludes": [
    ],
    "sass": {},
    "env": {
        "development": {
            "define": {
                "ENV": "test",
            },
            "publicPath": "/",
            "extraBabelPlugins": [
                "dva-hmr"
            ]
        },
        "production": {
            "publicPath": "/",
            "extraBabelPlugins": [
                "transform-runtime"
            ]
        }
    },
    "extraBabelPlugins": [
        [
            "import", {
                "libraryDirectory": "es",
                "libraryName": "antd",
                "style": "css"
            }
        ],
        [
            "react-intl", {
                "messagesDir": "./messages/"
            }
        ]
    ],
    "proxy": proxyObj,
    "disableDynamicImport": false,
    "disableCSSSourceMap": true,
    "disableCSSModules": true,
    "ignoreMomentLocale": true,
};

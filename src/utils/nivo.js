import {each, sortBy, filter, keys, uniq, has, reduce, isEmpty, isFunction} from 'lodash';
import moment from 'moment';

export const COLORS = [
    '#0190fe',
    '#7dd3ae',
    'yellow',
    '#926dde',
    '#46be8a',
    '#fb434a',
    '#9daaf3',
    '#83b944',
    '#f6be80',
    '#9ae1e9',
];

export function getData(labelAccessor, data) {
    // collect all of the dates (only uinque values)
    let dates = uniq(reduce(data, (array, row) => {
        each(row, (value, key) => {
            if (moment(key, 'YYYY-MM-DD', true).isValid()) {
                array.push(key);
            }
        });
        return array;
    }, []));

    // sort list
    dates = sortBy(dates, (date) => moment(date).valueOf());

    // get results
    const array = data.map((row, i) => {
        const data = dates.map((key) => ({
            x: key,
            y: has(row, key) ? +row[key] : null,
        }));

        return {
            id: isFunction(labelAccessor) ? labelAccessor(row) : row[labelAccessor],
            color: COLORS[i],
            data: data,
        };
    });

    // epmpty set
    if (isEmpty(array)) {
        return [{id: 'empty', data: [], color: 'rgba(0,0,0,1)'}]
    }

    return array;
}


export function getMin(markers, data) {


    // empty set
    if (isEmpty(data)) {
        return 'auto'
    }

    // collect all of the prices
    let prices = reduce(data, (array, row) => {
        each(row, (value, key) => {
            if (moment(key, 'YYYY-MM-DD', true).isValid()) {
                array.push(value);
            }
        });
        return array;
    }, []);

    markers.forEach(function (item) {
        prices.push(item.value);
    })

    let min = Math.min(...prices);
    min = Math.round(min) - 4;

    return min;
}

export function getMax(markers, data) {

    // empty set
    if (isEmpty(data)) {
        return 'auto'
    }

    // collect all of the prices
    let prices = reduce(data, (array, row) => {
        each(row, (value, key) => {
            if (moment(key, 'YYYY-MM-DD', true).isValid()) {
                array.push(value);
            }
        });
        return array;
    }, []);

    markers.forEach(function (item) {
        prices.push(item.value);
    });

    let max = Math.max(...prices);
    max = Math.round(max) + 4;

    return max;
}

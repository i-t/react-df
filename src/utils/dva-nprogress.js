import nprogressDva from 'dva-nprogress';

export default ((types = {}) => {
    const {
        onEffect,
        onAction
    } = nprogressDva({
        types,
    });

    return {
        onEffect: function (effect, {
            put
        }, model, actionType) {
            return function* (...args) {
                if (~types.indexOf(actionType)) {
                    yield(onEffect(effect, {
                        put
                    }, model, actionType)(...args));
                } else {
                    yield effect(...args);
                }
            };
        },
        onAction,
    };
});
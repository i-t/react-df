import 'babel-polyfill';
import 'nprogress/nprogress.css';
import dva from 'dva';
import createHistory from 'history/createHashHistory';
import nprogressDva from 'utils/dva-nprogress';
import {onError} from 'utils/error';
import {each} from 'lodash';
import {storageEnhancer} from 'dva-redux-persist';
import * as FastClick from 'fastclick';
import router from './router';
import * as models from './models';
import './resources/_antd.less';
import 'bootstrap/dist/css/bootstrap.min.css';
import './resources/AntStyles/AntDesign/antd.cleanui.scss';
import './resources/CleanStyles/Core/core.cleanui.scss';
import './resources/CleanStyles/Vendors/vendors.cleanui.scss';
import localForage from 'localforage';

// Application
const document = window.document || {};
const setImmediate = (window.setImmediate || window.requestAnimationFrame || window.setTimeout).bind(window);
const app = dva({
    initialState: {},
    history: createHistory(),
    extraEnhancers: [
        storageEnhancer({
            storage: localForage,
            blacklist: ['location', 'product', 'report', 'retailer', 'message', 'channel', 'reportBuyBox'],
        }),
    ],
    onStateChange() {

    },
    onError: onError(),
    onReducer: reducer => (state, action) => {
        if (action.type === 'persist/REHYDRATE') {
            setImmediate(() => app._store.dispatch({type: 'user/REHYDRATE'}));
        }
        // cleanup state on user.logout action.
        return reducer((action.type === 'user/LOGOUT') ? {} : state, action);
    },
});

// 2. Plugins
app.use(nprogressDva([
    'location/FILTER/@@start',
]));

// 3. Models
each(models, model => model.namespace && app.model(model));

// 4. Router
app.router(router);

// 5. Start
FastClick.attach(document.body);
app.start('#root');

export default app;

import React from 'react'
import {connect} from 'react-redux'
import {Menu, Dropdown, Avatar, Badge} from 'antd'
import {Link} from 'react-router-dom';

const mapDispatchToProps = dispatch => ({
    logout: event => {
        event.preventDefault()
        dispatch({
            type: 'user/LOGOUT',
        });
    },
});

const mapStateToProps = ({user, message}, props) => ({user, message})

@connect(mapStateToProps, mapDispatchToProps)
class ProfileMenu extends React.Component {
    render() {
        const {user, logout, message} = this.props
        const {customer} = user;

        const menu = (
            <Menu selectable={false}>
                <Menu.Item>
                    <div className="rfq__widget__system-status__item">
                        <strong>Hello, {user.name}!</strong>
                    </div>
                </Menu.Item>
                <Menu.Divider/>
                <Menu.Item>
                    <div className="rfq__widget__system-status__item">
                        <strong>Email:</strong> {user.email}
                        <br/>
                        <strong>Phone:</strong> {user.phone}
                        <br/>
                        {customer && <span><strong>Company:</strong> {customer.company}</span>}
                    </div>
                </Menu.Item>
                <Menu.Divider/>
                <Menu.Item>
                    <Link to="/profile">
                        <i className="topbar__dropdownMenuIcon icmn-user"/> Edit Profile
                    </Link>
                </Menu.Item>
                <Menu.Divider/>
                <Menu.Item>
                    <a href="javascript: void(0);" onClick={logout}>
                        <i className="topbar__dropdownMenuIcon icmn-exit"/> Logout
                    </a>
                </Menu.Item>
            </Menu>
        );
        return (
            <div className="topbar__dropdown d-inline-block">
                <Dropdown
                    overlay={menu}
                    trigger={['click']}
                    placement="bottomRight"
                >
                    <a className="ant-dropdown-link" href="/">
                        <Badge>
                            <Avatar src={user.avatar} className="topbar__avatar" shape="square" size="large"
                                    icon="user"/>
                        </Badge>
                    </a>
                </Dropdown>
            </div>
        )
    }
}

export default ProfileMenu

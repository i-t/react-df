import { toMatrix } from 'utils/array';
import React from "react";
import { connect } from 'dva';
import ProfileMenu from "./ProfileMenu";
import { Select } from "antd";
import "./style.scss";

const Option = Select.Option;

const mapStateToProps = ({user}) => ({user});

@connect(mapStateToProps)
class TopBar extends React.Component {
  state = {
    selectedBrandAuthToken: ''
  };
  onSearch = (query) => {
    return this.props.dispatch({
      type: 'product/FIND',
      query: query,
    }).then((array) => {
      return toMatrix(array, 3, 3, ({verified_sku}) => ({
        text: verified_sku,
        link: `/products/${verified_sku}`,
      }), (count) => ({
        text: `and ${count} more`,
      }));
    });
  }
  handleChangeBrand = (auth_token) => {
    const _authToken = auth_token ? auth_token : '';
    localStorage.setItem("selectedBrandAuthToken", _authToken);
    this.setState({selectedBrandAuthToken: _authToken});
    setTimeout(() => {
      window.location.reload(); // important for refresh all states for selected auth token
    }, 200);
  };

  render() {
    const { isAdmin, customer } = this.props.user;
    let customerType = 'Brand';
    let brands = [];
    let defaultBrandAuthToken = '';
    if (customer && customer.type === 'Agency') {
      customerType = customer.type;
      defaultBrandAuthToken = localStorage.getItem('selectedBrandAuthToken');
      brands = customer.brands && customer.brands.name_auth_token ? JSON.parse(customer.brands.name_auth_token) : [];
      let changeAuthToken = false;
      brands.map(brand => {
        if (brand['auth_token'] === defaultBrandAuthToken) {
          changeAuthToken = true;
        }
        brand['key'] = (Math.random() + Date.now().toString()).toString();
        return brand;
      });
      defaultBrandAuthToken = changeAuthToken ? defaultBrandAuthToken : '';
    }
    return (
      <div className="topbar">
        <div className="topbar__left">
          {(isAdmin !== 0) && (
            <span>
              <strong>Success!</strong>{' '}
              You have been logged in as Administrator.
            </span>
          )}
        </div>
        <div className="topbar__right">
          {!isAdmin && customerType === 'Agency' &&
            <Select defaultValue={defaultBrandAuthToken} style={{width: 240, marginRight: 10}} onChange={this.handleChangeBrand}>
              <Option value="">-- Select Brand --</Option>
              {brands.map(brand => {
                return (
                    <Option value={brand['auth_token']} key={brand['key']}>{brand['name']}</Option>
                );
              })}
            </Select>
          }
          <ProfileMenu />
        </div>
      </div>
    );
  }
}

export default TopBar;

import React from "react";
import {Link} from 'react-router-dom'
import {connect} from 'dva';
import "./style.scss";

const mapStateToProps = ({user}) => ({user});

@connect(mapStateToProps)
class AppFooter extends React.Component {
    render() {
        const {customer} = this.props.user;
        const isRequestUploadAvailable = customer ? (customer.other.indexOf('RU') > -1) : false
        return (
            <div className="footer">
                <div className="footer__top">
                    <div className="row">
                        <div className="col-lg-9 mb-3">
                            <div className="mb-3">
                                <strong>About TestIQ</strong>
                            </div>
                            <div className="cat__footer__description">
                                <p>
                                    TestIQ
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="footer__bottom">
                    {isRequestUploadAvailable &&
                    <div className="row">
                        <div className="col-md-4">
                            <Link to="/requestUpload" className="btn btn-default btn-rounded">
                                Request Upload
                            </Link>
                        </div>
                        <div className="col-md-8">
                            <div className="footer__copyright">
                                <img
                                    className="cat__footer__company-logo"
                                    src="resources/images/bottom_logo.png"
                                    title="TestIQ"
                                    alt="TestIQ"
                                />
                                <span>
                  © 2019{' '}
                                    <a href="#" target="_blank" rel="noopener noreferrer">
                    TestIQ
                  </a>
                  <br/>
                  All rights reserved
                </span>
                            </div>
                        </div>
                    </div>
                    }
                    {!isRequestUploadAvailable &&
                    <div className="row">
                        <div className="col-md-12">
                            <div className="footer__copyright footer__copyright--single">
                                <img
                                    className="cat__footer__company-logo"
                                    src="resources/images/bottom_logo.png"
                                    title="TestIQ"
                                    alt="TestIQ"
                                />
                                <span>
                  © 2019{' '}
                                    <a href="#" target="_blank" rel="noopener noreferrer">
                    TestIQ
                  </a>
                  <br/>
                  All rights reserved
                </span>
                            </div>
                        </div>
                    </div>
                    }
                </div>
            </div>
        );
    }
}

export default AppFooter;

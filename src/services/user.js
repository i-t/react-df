import {request} from 'utils/request';

export async function login(username, password) {
    const sign = btoa(`${username}:${password}`);

    return await request('user/login?expand=customer', {
        method: 'POST',
        headers: {
            Authorization: `Basic ${sign}`,
        },
    });
}

export async function update(id, data) {
    return await request(`users/${id}`, {
        method: 'PUT',
        body: JSON.stringify(data),
    });
}

export async function me() {
    return await request('user/me?expand=customer');
}

export async function requestUnlock(username) {
}

export async function forgotPassword(username) {
    return await request(`user/forgot?username=${encodeURIComponent(username)}`);
}

export async function load({isGuest, isAdmin}) {
    if (isGuest) {
        return [];
    } else if (isAdmin) {
        return [{
            title: 'Messages',
            key: 'messages',
            icon: 'icmn icmn-bubbles2',
            url: '/messages',
            access: [],
        }, {
            title: 'User management',
            key: 'userManagement',
            icon: 'icmn icmn-home',
            children: [
                {
                    title: 'Customers',
                    key: 'customers',
                    url: '/admin/customers',
                    access: [],
                },
                {
                    title: 'Users',
                    key: 'users',
                    url: '/admin/users',
                    access: [],
                },
                {
                    title: 'Admins',
                    key: 'admins',
                    url: '/admin/admins',
                    access: [],
                },
            ],
        },
            {
                title: 'Quality Control',
                key: 'quality-control',
                icon: 'icmn icmn-ticket',
                url: '/admin/quality-control',
                access: [],
            },];
    } else {
        return [
            {
                title: 'Dashboard',
                key: 'dashboard',
                url: '/',
                icon: 'icmn icmn-home',
                access: [],
            },
            {
                divider: true,
            },
            {
                title: 'MAP Violations',
                key: 'mapViolations',
                url: '/reports/mapViolations',
                icon: 'icmn icmn-map',
                access: [],
            },
            {
                title: 'Reviews',
                key: 'reviews',
                icon: 'icmn icmn-bubble custom-bubble',
                access: ['reports:RE'],
                children: [
                    {
                        title: 'All Reviews',
                        key: 'allReviews',
                        url: '/reports/allReviews',
                        access: ['reports:RE'],
                    },
                    {
                        title: 'Positive Reviews',
                        key: 'positiveReviews',
                        url: '/reports/positiveReviewsByDate',
                        access: ['reports:RE'],
                    },
                    {
                        title: 'Negative Reviews',
                        key: 'negativeReviews',
                        url: '/reports/negativeReviewsByDate',
                        access: ['reports:RE'],
                    }
                ]
            },
            {
                title: 'Products',
                key: 'products',
                url: '/products',
                icon: 'icmn icmn-stack',
                access: [],
            },
            {
                divider: true,
            },
            {
                title: 'Reports',
                key: 'reports',
                icon: 'icmn icmn-file-text',
                children: [
                    {
                        title: 'All Product Listings',
                        key: 'allProductListings',
                        url: '/reports/allProductListings',
                        access: ['reports:AP'],
                    },
                    {
                        title: 'Pricing Report',
                        key: 'pricingReport',
                        url: '/reports/pricingReport',
                        access: ['reports:PR'],
                    },
                    {
                        title: 'Buy Box',
                        key: 'buyBox',
                        url: '/reports/buyBox',
                        access: ['reports:BB'],
                    },
                    {
                        title: 'Out of Stock',
                        key: 'outStock',
                        url: '/reports/outStock',
                        access: ['reports:OS'],
                    },
                    {
                        title: 'In Stock',
                        key: 'inStock',
                        url: '/reports/inStock',
                        access: ['reports:IS'],
                    },
                    {
                        title: 'Low Image Count',
                        key: 'lowImage',
                        url: '/reports/lowImage',
                        access: ['reports:LI'],
                    },
                    {
                        title: 'No MFR SKU',
                        key: 'noMfrSku',
                        url: '/reports/noMfrSku',
                        access: ['reports:NM'],
                    },
                    {
                        title: 'Inventory / Forecast',
                        key: 'inventoryForecast',
                        url: '/reports/inventoryForecast',
                        access: ['reports:FR'],
                    },
                    {
                        title: 'Inventory Needed',
                        key: 'inventoryNeeded',
                        url: '/reports/inventoryNeeded',
                        access: ['reports:IN'],
                    },
                    {
                        title: 'No Cover',
                        key: 'noCover',
                        url: '/reports/noCover',
                        access: ['reports:NC'],
                    },
                    {
                        title: 'Price Drop',
                        key: 'priceDrop',
                        url: '/reports/priceDrop',
                        access: ['reports:PD'],
                    },
                ],
            },
            {
                title: 'Competition',
                key: 'competitionParent',
                icon: 'icmn icmn-binoculars',
                access: ['reports:CS'],
                children: [
                    {
                        title: 'Competition Chart',
                        key: 'competition',
                        url: '/competitionSKU',
                    },
                    {
                        title: 'Competitor Details',
                        key: 'compPriceData',
                        url: '/reports/compPriceData',
                    },
                ],
            },
            {
                title: 'Retailers',
                key: 'retailers',
                icon: 'icmn icmn-office',
                url: '/retailers',
                access: [],
            },
            {
                title: 'Competition SKU',
                key: 'competition',
                icon: 'icmn icmn-arrow-up-right',
                url: '/competitionSKU',
                access: ['reports:CS'],
            },
            {
                divider: true,
            },
            {
                title: 'Request Upload',
                key: 'requestUpload',
                icon: 'icmn icmn-upload',
                url: '/requestUpload',
                access: ['other:RU'],
            },
            {
                title: 'Messages',
                key: 'messages',
                icon: 'icmn icmn-bubbles2',
                url: '/messages',
                access: [],
            },
            {
                title: 'Alerts',
                key: 'alerts',
                url: '/alerts',
                icon: 'icmn icmn-notification',
                access: [],
            }
        ];
    }
}

import React from 'react';
import {Helmet} from 'react-helmet';
import {Layout} from 'antd';
import {injectIntl} from 'react-intl';
import {connect} from 'dva';

const mapStateToProps = ({user}) => ({user});

@connect(mapStateToProps)
class UnderlyingLayout extends React.Component {
    render() {
        const {children} = this.props;
        return (
            <Layout>
                <Helmet titleTemplate="PricingIQ | %s" bodyAttributes={{'class': 'borderLess menuShadow'}}
                        title="Home"/>
                <Layout.Content>
                    {children}
                </Layout.Content>
            </Layout>
        )
    }
}

export const BaseLayout = injectIntl(UnderlyingLayout);
import React from 'react';
import {injectIntl} from 'react-intl';
import {BaseLayout} from './BaseLayout';
import './GuestLayout.scss';

class UnderlyingLayout extends BaseLayout {
    render() {
        const {children} = this.props;
        return (
            <BaseLayout>
                <div className="guestLayout">
                    <div className="guestLayout__header">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="guestLayout__header__logo">
                                    <a href="javascript: void(0);">
                                        <img src="resources/images/logo.png" alt="PricingIQ"/>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="guestLayout__content">
                        {children}
                    </div>
                    <div className="guestLayout__footer text-center">
                        <p>
                            &copy; 2019 PricingIQ. All rights reserved.
                        </p>
                    </div>
                </div>
            </BaseLayout>
        )
    }
}

export const GuestLayout = injectIntl(UnderlyingLayout);

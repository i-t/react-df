import React from 'react';
import { injectIntl } from 'react-intl';
import { BaseLayout } from './BaseLayout';

class UnderlyingLayout extends BaseLayout {
  render() {
    const { children } = this.props;
    return (
      <BaseLayout>
        {children}
      </BaseLayout>
    );
  }
}

export const AdminLayout = injectIntl(UnderlyingLayout);

import pathToRegExp from 'path-to-regexp';
import {routerRedux} from 'dva/router';
import {some} from 'lodash';
import {
    update as updateUser, login, requestUnlock, forgotPassword, me,
} from 'services/user';
import {getCustomer} from 'services/customer';
import {headers, payload as dataPayload} from 'utils/request';

export const LOGIN_ROUTE = '/login';
export const PUBLIC_ROUTES = ['/login', '/sign-up', '/forgot'];
export const User = {
    namespace: 'user',
    state: {
        id: null,
        name: '',
        language: 'en',
        isGuest: null,
        returnUrl: null,
        errors: [],
        isUpdating: false,
    },
    reducers: {
        SET_STATE: (state, action) => {
            return {...state, ...action.payload};
        },
    },
    effects: {
        * AUTHORIZED(action, saga) {

        },
        * REHYDRATE({payload}, saga) {
            yield saga.put({
                type: 'ME',
            });
        },
        * ME({payload}, saga) {
            // FIRST - need to ensure, that credentials are valid.
            yield saga.put({
                type: 'IS_GUEST',
                * success() {
                    // Try to query new data / verify credentials.
                    let data = {id: 0, isGuest: true};
                    try {
                        data = yield saga.call(me);
                    } catch (e) {
                    }

                    // empty response?
                    if (!data) {
                        data = {id: 0, isGuest: true};
                    } else {
                        if (!data.isAdmin) {
                            // checking customer status
                            if (!data.customer || data.customer.status !== 'active') { // The User cannot login if customer is inactive
                                yield saga.put({
                                    type: 'LOGOUT',
                                });
                                return {customerStatus: false};
                            }
                        }
                        yield saga.put({
                            type: 'CHECK_FIRST_LOGIN'
                        });
                    }

                    // Update state with new data.
                    yield saga.put({
                        type: 'SET_STATE',
                        payload: data,
                    });

                    // Ensure, that latest state still correct.
                    yield saga.put({
                        type: 'IS_GUEST',
                        * success() {
                            yield saga.put({
                                type: 'AUTHORIZED',
                            });
                        },
                    });
                },
            });
        },
        * LOGIN({payload}, saga) {
            const {username, password} = payload;
            const response = yield saga.call(login, username, password);
            const returnUrl = yield saga.select(state => (state.user || {returnUrl: '/'}).returnUrl);

            if (response && !response.isAdmin) {
                // checking customer status
                if (!response.customer || response.customer.status !== 'active') { // The User cannot login if customer is inactive
                    return {customerStatus: false};
                }
            }

            yield saga.put({
                type: 'CHECK_FIRST_LOGIN'
            });

            yield saga.put({
                type: 'SET_STATE',
                payload: response,
            });

            yield saga.put({
                type: 'IS_GUEST',
                payload: {pathname: returnUrl},
                * success({id}) {
                    yield saga.put({
                        type: 'AUTHORIZED',
                    });
                    yield saga.put(routerRedux.push((response && response.isAdmin) ? '/admin/customers' : returnUrl));
                },
            });

            return response;
        },
        * LOGOUT(action, {all, put}) {
            yield all([
                put({
                    type: 'SET_STATE',
                    payload: {
                        id: 0,
                    },
                }),
                put({
                    type: 'IS_GUEST',
                    payload: {},
                }),
            ]);
        },
        * REQUEST_UNLOCK({payload: {username}}, saga) {
            return yield saga.call(requestUnlock, username);
        },
        * FORGOT({payload: {username}}, saga) {
            return yield saga.call(forgotPassword, username);
        },
        * SET_LANGUAGE({payload}, saga) {
            yield saga.put({
                type: 'SET_STATE',
                payload: {
                    language: payload,
                },
            });
        },
        * CHECK_FIRST_LOGIN({payload}, {select, put}) {
            const user = yield select(({user}) => user);

            if (user && user.id !== null && user.firstLogged === 0) {
                const pathname = (payload && payload.pathname) ? payload.pathname : user.returnUrl;
                let isPublic = undefined;
                if (payload && payload.isPublic !== undefined) {
                    isPublic = payload.isPublic;
                } else {
                    isPublic = some(PUBLIC_ROUTES, (entry) => {
                        const regex = entry.replace(/(?=\W)/g, '\\').replace(/$/, '(\\?[^?]*|)$');
                        return new RegExp(`^${regex}`, 'i').test(pathname);
                    });
                }

                if (!isPublic && pathname !== '/profile') {
                    yield put(routerRedux.push(`/profile`)); // redirecting to force change password for first login
                }
            }
        },
        * UPDATE({payload}, {select, call, put}) {
            yield put({
                type: 'SET_STATE',
                payload: {isUpdating: true},
            });

            const user = yield select(({user}) => user);
            const response = yield call(updateUser, user.id, payload);
            yield put({
                type: 'SET_STATE',
                payload: {...user, ...response, isUpdating: false},
            });
            return response;
        },
        * CHANGE_CUSTOMER({payload, success}, {select, put, call}) {
            yield put({
                type: 'SET_STATE',
                payload: {isUpdating: true},
            });

            const resolve = function* () {
                const user = yield select(({user}) => user);
                if (user && user.accessToken) {
                    headers.Authorization = `Bearer ${user.accessToken}`;
                }
                const customerId = localStorage.getItem('selectedCustomerId');
                if (user.isAdmin && customerId) {
                    if (!user.customer || user.customer.id !== customerId) { // important for getting needed customer one time
                        const entryCustomer = yield call(getCustomer, customerId);
                        if (entryCustomer) {
                            dataPayload.authToken = entryCustomer.authToken;
                            user.customer = entryCustomer;
                            user.customerId = entryCustomer.id;
                            yield put({
                                type: 'SET_STATE',
                                payload: {...user},
                            });
                        }
                    } else if (user.customer) {
                        dataPayload.authToken = user.customer.authToken;
                    }
                }
                if (success) {
                    return yield success({...user});
                }
                return user;
            };

            return yield resolve();
        },
        * IS_GUEST({
                       payload, success, fail, ...data
                   }, saga) {
            const {pathname} = payload || {};
            const returnUrl = pathname || '/';
            const user = yield saga.select(({user}) => user);
            const regex = pathToRegExp(LOGIN_ROUTE);
            const isGuest = (user.id !== null && user.id <= 0);
            const {pathname: currentPathname} = yield saga.select(({location}) => location);
            const isRedirect = !regex.test(currentPathname);
            const isPublicRoute = PUBLIC_ROUTES.some(route => new RegExp(`^${route.replace(/(?=\W)/g, '\\')}`, 'i').test(currentPathname));
            const resolve = function* () {
                const {accessToken, customer, isAdmin} = yield saga.select(({user}) => user);
                if (accessToken) {
                    headers.Authorization = `Bearer ${accessToken}`;
                }
                if (customer) {
                    dataPayload.authToken = customer.authToken;
                    if (!isAdmin && customer.type === 'Agency') {
                        const selectedBrandAuthToken = localStorage.getItem('selectedBrandAuthToken');
                        if (selectedBrandAuthToken) {
                            let changeAuthToken = false;
                            if (customer.brands && customer.brands && customer.brands.name_auth_token) {
                                const brands = JSON.parse(customer.brands.name_auth_token);
                                brands.forEach(brand => {
                                    if (brand['auth_token'] === selectedBrandAuthToken) {
                                        changeAuthToken = true;
                                    }
                                });
                            }
                            if (changeAuthToken) {
                                dataPayload.authToken = selectedBrandAuthToken;
                            }
                        }
                    }
                }
                if (success) {
                    yield success({...user, pathname: currentPathname});
                }
            };

            const reject = function* () {
                const url = encodeURIComponent(returnUrl);
                if (isRedirect && !isPublicRoute) {
                    yield saga.put(routerRedux.push(`${LOGIN_ROUTE}?returnUrl=${url}`));
                }

                if (fail) {
                    yield fail({...user, pathname: currentPathname});
                }
            };

            if (user.isGuest !== isGuest || user.returnUrl !== returnUrl) {
                yield saga.put({
                    type: 'SET_STATE',
                    payload: {
                        isGuest,
                        returnUrl,
                    },
                });
            }

            yield (isGuest ? reject() : resolve());
        },
    },
    subscriptions: {
        setup: ({dispatch, history}) => {
            const handler = ({pathname}) => {
                const isPublic = some(PUBLIC_ROUTES, (entry) => {
                    const regex = entry.replace(/(?=\W)/g, '\\').replace(/$/, '(\\?[^?]*|)$');
                    return new RegExp(`^${regex}`, 'i').test(pathname);
                });
                dispatch({type: 'CHECK_FIRST_LOGIN', payload: {pathname: pathname, isPublic: isPublic}});
                if (!isPublic) {
                    dispatch({
                        type: 'IS_GUEST',
                        payload: {
                            pathname,
                        },
                    });
                }
            };
            history.listen(handler);
        }
    },
};

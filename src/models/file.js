import {uploadFile} from '../services/file';

export const File = {
    namespace: 'file',
    state: {},
    reducers: {},
    effects: {
        * UPLOAD({payload: {file}}, {call}) {
            const response = yield call(uploadFile, file, file.name);
            return response;
        }
    }
};
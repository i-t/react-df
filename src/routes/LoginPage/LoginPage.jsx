import React from 'react'
import {connect} from 'dva'
import {LoginForm} from './LoginForm'
import {RestorePasswordForm} from './RestorePasswordForm'
import {Helmet} from 'react-helmet'
import {notification} from 'antd'
import {routerRedux} from 'dva/router'
import './LoginPage.scss'

@connect()
export class LoginPage extends React.Component {
    isActive = true;
    state = {
        isLoading: false
    };

    onSubmit = values => {
        const {dispatch} = this.props;
        const done = (res) => {
            if (this.isActive) {
                this.setState({isLoading: false});
            }
            if (res && res.customerStatus === false) {
                notification.warning({
                    message: 'Info',
                    description: "Your customer is inactive and you cannot login now.",
                });
            }
        };

        this.setState({isLoading: true});
        dispatch({
            type: 'user/LOGIN',
            payload: values
        }).then(done, done);
    };


    componentWillUnmount() {
        this.isActive = false;
    }

    componentDidMount() {
        const email = (this.props.match.params || {}).email;

        if (email) {
            notification.success({
                message: 'Success',
                description: `New password was successfully sent to ${email}`,
            });
        }
    }

    onForgot = values => {
        const {dispatch} = this.props;
        const done = (data) => {
            if (data && data.success) {
                notification.success({
                    message: 'Success',
                    description: 'Restore password link was successfully sent to your email'
                });
                dispatch(routerRedux.push(`/login`));
            } else if (data && data.message) {
                notification.warning({
                    message: 'Error',
                    description: data.message,
                });
            }
            this.setState({isLoading: false});
        };

        this.setState({isLoading: true});
        dispatch({
            type: 'user/FORGOT',
            payload: values
        }).then(done, done)
    };

    render() {
        const {isLoading} = this.state;
        const isForgot = (/forgot/i.test(this.props.match.url));
        const Form = (isForgot) ? RestorePasswordForm : LoginForm;

        return (
            <div>
                <Helmet bodyAttributes={{'class': ''}}/>
                <div className="loginPage__block">
                    <div className="row">
                        <div className="col-xl-12">
                            <div className="loginPage__block__inner">
                                <div className="loginPage__block__form">
                                    <Form onSubmit={isForgot ? this.onForgot : this.onSubmit} isLoading={isLoading}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
